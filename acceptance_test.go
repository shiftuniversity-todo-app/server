package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	. "server/src/handlers"
	. "server/src/types"
	"strconv"

	"testing"

	"github.com/gofiber/fiber/v2"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAcceptance_QuestionsGet(t *testing.T) {

	Convey("Given todo model", t, func() {
		m1 := Model{ID: "1", Todo: "bla", V: 0}
		m2 := Model{ID: "2", Todo: "bla bla", V: 0}
		m3 := Model{ID: "3", Todo: "bla bla bla", V: 0}

		AddTodo(&m1)
		AddTodo(&m2)
		AddTodo(&m3)

		Convey("When I GET request to /todos", func() {
			request, _ := http.NewRequest(http.MethodGet, "/todos", nil)

			request.Header.Add("Content-Type", "application/json")
			app := fiber.New()
			app = Endpoints(app)

			response, err := app.Test(request, 30000)

			So(err, ShouldBeNil)

			Convey("Then should return status code 200", func() {
				So(response.StatusCode, ShouldEqual, fiber.StatusOK)

				Convey("Then should return todos list", func() {
					resBody, err := ioutil.ReadAll(response.Body)

					So(err, ShouldBeNil)

					data := List{}

					err = json.Unmarshal(resBody, &data)

					So(err, ShouldBeNil)
					So(data.Todos[0].ID, ShouldEqual, m1.ID)
					So(data.Todos[1].ID, ShouldEqual, m2.ID)
					So(data.Todos[2].ID, ShouldEqual, m3.ID)
					So(data.Todos[0].Todo, ShouldEqual, m1.Todo)
					So(data.Todos[1].Todo, ShouldEqual, m2.Todo)
					So(data.Todos[2].Todo, ShouldEqual, m3.Todo)
				})
			})

		})

	})
}

func TestAcceptance_QuestionsPost(t *testing.T) {
	Convey("Given add todo body", t, func() {
		m := Model{ID: "1", Todo: "bla"}

		Convey("When I send a POST request to /todos error should be nil", func() {
			todoByte, err := json.Marshal(m)
			So(err, ShouldBeNil)

			todoReader := bytes.NewReader(todoByte)
			request, err := http.NewRequest(http.MethodPost, "/todos", todoReader)
			So(err, ShouldBeNil)

			request.Header.Add("Content-Type", "application/json")
			request.Header.Set("Content-Length", strconv.Itoa(len(todoByte)))

			app := fiber.New()
			app = Endpoints(app)

			response, err := app.Test(request, 20000)
			So(err, ShouldBeNil)

			Convey("Then status code should be 201", func() {
				So(response.StatusCode, ShouldEqual, fiber.StatusCreated)
			})

		})
	})
}
