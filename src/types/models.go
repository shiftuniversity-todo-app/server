package types

type Model struct {
	ID   string `json:"_id" bson:"_id"`
	Todo string `json:"todo" bson:"todo"`
	V    int    `json:"_v" bson:"_v"`
}

type List struct {
	Todos []Model `json:"todos"`
}
