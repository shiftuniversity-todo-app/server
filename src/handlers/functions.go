package handlers

import (
	"os"

	. "server/src/connection"
	. "server/src/types"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
)

func Endpoints(app *fiber.App) *fiber.App {

	app.Get("/todos", GetHandler)
	app.Post("/todos", PostHandler)

	return app
}

func GetHandler(c *fiber.Ctx) error {
	mode := os.Getenv("MODE")

	var todos *List
	var err error

	if mode == "TEST" {
		todos, err = MockGetTodos()
	} else {
		todos, err = GetTodos()
	}

	c.JSON(todos)
	c.Status(fiber.StatusOK)
	return err
}

func MockGetTodos() (*List, error) {
	m1 := Model{ID: "1", Todo: "bla", V: 0}
	m2 := Model{ID: "2", Todo: "bla bla", V: 0}
	m3 := Model{ID: "3", Todo: "bla bla bla", V: 0}

	todoList := List{
		Todos: []Model{m1, m2, m3},
	}

	return &todoList, nil
}

func GetTodos() (*List, error) {
	client, ctx := MongoClient()
	db := client.Database("shiftTodo").Collection("todos")

	todoList := List{}

	cursor, err := db.Find(ctx, bson.M{})

	if err != nil {
		return nil, err
	}

	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		model := Model{}

		cursor.Decode(&model)
		todoList.Todos = append(todoList.Todos, model)
	}

	return &todoList, nil
}

func PostHandler(c *fiber.Ctx) error {
	mode := os.Getenv("MODE")

	model := Model{}

	err := c.BodyParser(&model)

	if err != nil {
		return err
	}

	var data *Model
	var err2 error

	if mode == "TEST" {
		c.Status(fiber.StatusCreated)
	} else {
		err2 = AddTodo(&model)
	}

	c.JSON(data)
	c.Status(fiber.StatusCreated)
	return err2

}

func AddTodo(m *Model) error {
	if m.Todo == "" {
		return fiber.NewError(404, "Todo should not be empty")
	}

	client, ctx := MongoClient()
	db := client.Database("shiftTodo").Collection("todos")

	if m.ID == "" {
		m.ID = uuid.New().String()
	}

	db.InsertOne(ctx, m)

	return nil

}
