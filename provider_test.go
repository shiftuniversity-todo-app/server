package main

import (
	"testing"

	. "server/src/handlers"

	"github.com/gofiber/fiber/v2"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
)

func TestProvider(t *testing.T) {
	pact := &dsl.Pact{
		Provider: "todo-app-api",
	}

	pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            "http://127.0.0.1:8081",
		BrokerURL:                  "https://sgumusmerinosu.pactflow.io",
		BrokerToken:                "MKAth8_ToVH_8nxs3O59Gw",
		PublishVerificationResults: true,
		ProviderVersion:            "todo-app-provider-v0.0.1",

		BeforeEach: func() error {
			app := fiber.New()
			app = Endpoints(app)
			go app.Listen(":8081")

			return nil
		},
		StateHandlers: types.StateHandlers{
			"All questions": func() error {
				return nil
			},
			"Post a new question": func() error {
				return nil
			},
		},
	})
}
