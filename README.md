# Todo App Service 
- Production host: http://34.116.219.66/todos
- Test host: http://34.118.125.43/todos
---
## Technologies
- GoLang
- Fiber
- Pact
- google/uuid 
- MongoDB

## Setup and Usage
- Run `go mod download` for package installations.
- Run `go test -run TestUnit -v` for unit tests.
- Run `go test -run TestAcceptance  -v` for acceptance tests.
- Run `MODE=TEST go test -run TestProvider -v` for unit tests.
- Run `go build .` and then `./server` for run service.