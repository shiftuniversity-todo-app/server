package main

import (
	. "server/src/connection"
	. "server/src/handlers"
	. "server/src/types"

	. "github.com/smartystreets/goconvey/convey"

	"testing"
)

func TestUnit_DBConnection(t *testing.T) {
	Convey("MongoDB connect test", t, func() {
		client, ctx := MongoClient()

		So(ctx, ShouldNotBeNil)
		So(client, ShouldNotBeNil)
	})
}

func TestUnit_EmptyError(t *testing.T) {
	Convey("Empty model", t, func() {
		m := Model{}

		err := AddTodo(&m)

		So(err, ShouldNotBeNil)

	})
}
